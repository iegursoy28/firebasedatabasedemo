package tr.com.ieg.firebasedatabasedemo.viewholder;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import tr.com.ieg.firebasedatabasedemo.R;
import tr.com.ieg.firebasedatabasedemo.model.Post;

public class PostViewHolder extends RecyclerView.ViewHolder {
    private FirebaseStorage storage;
    private StorageReference storageRef;

    public ImageView authorImage;
    private TextView authorView;
    private TextView bodyView;
    private TextView titleView;

    public PostViewHolder(View itemView) {
        super(itemView);
        titleView = itemView.findViewById(R.id.post_title);
        authorView = itemView.findViewById(R.id.post_author);
        authorImage = itemView.findViewById(R.id.post_image);
        bodyView = itemView.findViewById(R.id.post_body);

        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
    }

    public void bindToPost(Post post, View.OnClickListener postClickListener) {
        titleView.setText(post.title);
        authorView.setText(post.author);
        bodyView.setText(post.body);

        StorageReference fileRef = storageRef.child(post.imagePath);
        final long ONE_MEGABYTE = 1024 * 1024;
        fileRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                // Data is returned in bytes array
                authorImage.setImageBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any error
            }
        });

        authorImage.setOnClickListener(postClickListener);
    }
}

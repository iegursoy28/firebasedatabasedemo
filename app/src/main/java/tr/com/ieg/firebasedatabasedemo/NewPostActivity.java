package tr.com.ieg.firebasedatabasedemo;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import tr.com.ieg.firebasedatabasedemo.model.Post;
import tr.com.ieg.firebasedatabasedemo.model.User;

public class NewPostActivity extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 123;

    private DatabaseReference mDatabase;
    private FirebaseStorage storage;
    private StorageReference storageRef;

    private EditText mTitleField;
    private EditText mBodyField;
    private FloatingActionButton mSubmitButton;
    private ImageView mImageField;

    private String imageLocalPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        mTitleField = (EditText) findViewById(R.id.np_et_title);
        mBodyField = (EditText) findViewById(R.id.np_et_body);
        mImageField = (ImageView) findViewById(R.id.np_iv_image);
        mSubmitButton = (FloatingActionButton) findViewById(R.id.np_fab_submit);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
            }
        });

        mImageField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                );

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imageLocalPath = cursor.getString(columnIndex);
            cursor.close();

            mImageField.setImageBitmap(BitmapFactory.decodeFile(imageLocalPath));
        }
    }

    private boolean validateForm(String title, String body) {
        if (TextUtils.isEmpty(title)) {
            mTitleField.setError("Required");
            return false;
        } else if (TextUtils.isEmpty(body)) {
            mBodyField.setError("Required");
            return false;
        } else {
            mTitleField.setError(null);
            mBodyField.setError(null);
            return true;
        }
    }

    private void submitPost() {
        final String title = mTitleField.getText().toString().trim();
        final String body = mBodyField.getText().toString().trim();
        final String userId = getUid();
        final String imagePath = imageLocalPath;

        if (validateForm(title, body)) {
            // Disable button so there are no multi-posts
            setEditingEnabled(false);
            mDatabase.child("users").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user == null) {
                        Toast.makeText(NewPostActivity.this, "Error: could not fetch user.", Toast.LENGTH_LONG).show();
                    } else {
                        writeNewPost(userId, user.username, title, body, imagePath);
                    }
                    setEditingEnabled(true);
                    finish();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    setEditingEnabled(true);
                    Toast.makeText(NewPostActivity.this, "onCancelled: " + databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private String uploadImage(String imageLocalPath) {
        String imageRemotePath = "images/" + System.currentTimeMillis();

        StorageReference riversRef = storageRef.child(imageRemotePath);
        UploadTask uploadTask = riversRef.putBytes(getImageByte(imageLocalPath));

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
            }
        });

        return imageRemotePath;
    }

    private byte[] getImageByte(String path) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        BitmapFactory.decodeFile(path).compress(Bitmap.CompressFormat.JPEG, 75, stream);

        return stream.toByteArray();
    }

    private void setEditingEnabled(boolean enabled) {
        mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
        if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }
    }

    private void writeNewPost(String userId, String username, String title, String body, String imagePath) {
        // Create new post at /user-posts/$userid/$postid
        // and at /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();
        Post post = new Post(userId, username, title, body, uploadImage(imagePath));
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
